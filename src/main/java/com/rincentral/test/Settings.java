package com.rincentral.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Settings {
    @Value("${external_server.host}")
    private  String host;
    @Value("${external_server.port}")
    private  String port;
    @Value("${external_server.ALL_CARS_URL}")
    private  String ALL_CARS_URL;
    @Value("${external_server.CAR_BY_ID_URL}")
    private  String CAR_BY_ID_URL;
    @Value("${external_server.ALL_BRANDS_URL}")
    private  String ALL_BRANDS_URL;


    public  String getAllCarsUrl() {
        return String.format("http://%s:%s/%s", host, port, ALL_CARS_URL);
    }

    public  String getCarByIdUrl() {
        return String.format("http://%s:%s/%s", host, port, CAR_BY_ID_URL);
    }

    public  String getAllBrandsUrl() {
        return String.format("http://%s:%s/%s", host, port, ALL_BRANDS_URL);
    }
}
