package com.rincentral.test.controllers;

import com.rincentral.test.models.CarInfo;
import com.rincentral.test.models.external.ExternalCarInfo;
import com.rincentral.test.models.external.enums.EngineType;
import com.rincentral.test.models.external.enums.FuelType;
import com.rincentral.test.models.external.enums.GearboxType;
import com.rincentral.test.models.external.enums.WheelDriveType;
import com.rincentral.test.models.params.CarRequestParameters;
import com.rincentral.test.models.params.MaxSpeedRequestParameters;
import com.rincentral.test.services.InternalCarsApiService;
import com.rincentral.test.services.MakerCarParameters;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

import static java.util.Collections.emptyList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApiController {

    @Autowired
    MakerCarParameters makerCarParameters;

    @Autowired
    InternalCarsApiService internalCarsApiService;

    private Boolean expression(CarRequestParameters requestParameters, ExternalCarInfo externalCarInfo) {

        Boolean ex = true;

        if (requestParameters.getCountry() != null) {
            ex = ex && (requestParameters.getCountry().equals(makerCarParameters.getCountry(externalCarInfo.getBrandId(), internalCarsApiService)));
        }

        if (requestParameters.getSegment() != null) {
            ex = ex && (requestParameters.getSegment().equals(externalCarInfo.getSegment()));
        }

        if (requestParameters.isSetMinEngineDisplacement()) {
            ex = ex && (requestParameters.getMinEngineDisplacement() <= externalCarInfo.getEngineDisplacement());
        }


        if (requestParameters.isSetMinEngineHorsepower()) {
            ex = ex && (requestParameters.getMinEngineHorsepower() <= externalCarInfo.getHp());
        }

        if (requestParameters.isSetMinMaxSpeed()) {
            ex = ex && (requestParameters.getMinMaxSpeed() <= externalCarInfo.getMaxSpeed());
        }

        if (requestParameters.isSetYear()) {

            Calendar calendar = Calendar.getInstance();
            Integer currentYear = calendar.get(Calendar.YEAR);
            String[] range = externalCarInfo.getYearsRange().split("-");
            Integer from = Integer.valueOf(range[0]);
            Integer to = range[1].equals("present") ? currentYear : Integer.valueOf(range[1]);
            ex = ex && (from < requestParameters.getYear() && requestParameters.getYear() <= to);
        }

        if (requestParameters.isSetBodyStyle()) {
            ex = ex && (externalCarInfo.getBodyStyle().toLowerCase().contains(requestParameters.getBodyStyle().toLowerCase()));
        }

        if (requestParameters.isSetSearch()) {

            ex = ex && (
                    (externalCarInfo.getModel().contains(requestParameters.getSearch()))
                            || (externalCarInfo.getGeneration().contains(requestParameters.getSearch()))
                            || (externalCarInfo.getModification().contains(requestParameters.getSearch()))
            );
        }

        return ex;
    }


    @GetMapping("/cars")
    public ResponseEntity<List<? extends CarInfo>> getCars(CarRequestParameters requestParameters) {

        if (requestParameters.isTypeMistake()) {
            ResponseEntity.status(BAD_REQUEST).body(emptyList());
        }

        List<CarInfo> cars = new ArrayList<>();

        for (Map.Entry<Integer, ExternalCarInfo> entry : internalCarsApiService.getExternalCarsInfoMap().entrySet()) {
            Integer id = entry.getKey();
            ExternalCarInfo externalCarInfo = entry.getValue();

            if (expression(requestParameters, externalCarInfo)) {
                cars.add(requestParameters.isSetIsFull() && requestParameters.getIsFull()
                        ? makerCarParameters.getCarFullInfo(id, internalCarsApiService)
                        : makerCarParameters.getCarInfo(id, internalCarsApiService)
                );
            }
        }


        return ResponseEntity.ok(cars);
    }

    @GetMapping("/fuel-types")
    public ResponseEntity<Set<FuelType>> getFuelTypes() {
        return ResponseEntity.ok(internalCarsApiService.getFuelTypeSet());
    }

    @GetMapping("/body-styles")
    public ResponseEntity<Set<String>> getBodyStyles() {
        return ResponseEntity.ok(internalCarsApiService.getBodyStylesSet());
    }

    @GetMapping("/engine-types")
    public ResponseEntity<Set<EngineType>> getEngineTypes() {
        return ResponseEntity.ok(internalCarsApiService.getEngineTypeSet());
    }

    @GetMapping("/wheel-drives")
    public ResponseEntity<Set<WheelDriveType>> getWheelDrives() {
        return ResponseEntity.ok(internalCarsApiService.getWheelDriveTypes());
    }

    @GetMapping("/gearboxes")
    public ResponseEntity<Set<GearboxType>> getGearboxTypes() {
        return ResponseEntity.ok(internalCarsApiService.getGearboxTypeSet());
    }

    @GetMapping("/max-speed")
    public ResponseEntity<Double> getMaxSpeed(MaxSpeedRequestParameters requestParameters) {
        if (requestParameters.isSetBrand() && requestParameters.isSetModel())
            return ResponseEntity.status(BAD_REQUEST).body(0.0);
        Double maxSpeed = 0D;
        int numbersOfCars = 0;


        for (Map.Entry<Integer, ExternalCarInfo> entry : internalCarsApiService.getExternalCarsInfoMap().entrySet()) {
            Integer id = entry.getKey();
            ExternalCarInfo externalCarInfo = entry.getValue();

            if (requestParameters.isSetBrand()
                    && internalCarsApiService
                    .getExternalBrandsMap()
                    .get(externalCarInfo.getBrandId())
                    .getTitle()
                    .equals(requestParameters.getBrand())
                    ||
                    requestParameters.isSetModel()
                            && externalCarInfo.getModel().equals(requestParameters.getModel())) {
                numbersOfCars++;
                maxSpeed += externalCarInfo.getMaxSpeed();
            }
        }
        System.out.println(numbersOfCars);
        if (numbersOfCars == 0) return ResponseEntity.status(NOT_FOUND).body(0.0);
        return ResponseEntity.ok(maxSpeed / numbersOfCars);
    }
}
