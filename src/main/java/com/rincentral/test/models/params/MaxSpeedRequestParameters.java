package com.rincentral.test.models.params;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MaxSpeedRequestParameters {

    private String brand=null;
    private String model=null;


    public Boolean isSetBrand() {
        return brand != null;
    }

    public Boolean isSetModel() {
        return model != null;
    }
}
