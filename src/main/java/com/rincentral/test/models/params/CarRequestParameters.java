package com.rincentral.test.models.params;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

//@Getter
//@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CarRequestParameters {
    private String country;
    private String segment;
    private Double minEngineDisplacement = null;
    private Integer minEngineHorsepower = null;
    private Integer minMaxSpeed = null;
    private String search;
    private Boolean isFull = null;
    private Integer year = null;
    private String bodyStyle = null;
    private Boolean typeMistake = false;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public Boolean isSetMinEngineDisplacement() {
        return minEngineDisplacement != null;
    }

    public Double getMinEngineDisplacement() {
        return minEngineDisplacement;
    }

    public void setMinEngineDisplacement(String minEngineDisplacement) {
        try {
            this.minEngineDisplacement = Double.valueOf(minEngineDisplacement);
        } catch (Exception e) {
            e.printStackTrace();
            this.minEngineDisplacement = null;
            typeMistake = true;
        }
    }

    public Boolean isSetMinEngineHorsepower() {
        return minEngineHorsepower != null;
    }

    public Integer getMinEngineHorsepower() {
        return minEngineHorsepower;
    }

    public void setMinEngineHorsepower(String minEngineHorsepower) {
        try {
            this.minEngineHorsepower = Integer.valueOf(minEngineHorsepower);
        } catch (Exception e) {
            e.printStackTrace();
            this.minEngineHorsepower = null;
            typeMistake = true;
        }
    }


    public Integer getMinMaxSpeed() {
        return minMaxSpeed;
    }

    public Boolean isSetMinMaxSpeed() {
        return minMaxSpeed != null;
    }

    public void setMinMaxSpeed(String minMaxSpeed) {
        try {
            this.minMaxSpeed = Integer.valueOf(minMaxSpeed);
        } catch (Exception e) {
            e.printStackTrace();
            this.minMaxSpeed = null;
            typeMistake = true;
        }
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Boolean isSetSearch() {
        return search != null;
    }

    public Boolean getIsFull() {
        return isFull;
    }

    public void setIsFull(String isFull) {
        try {
            this.isFull = Boolean.valueOf(isFull);
        } catch (Exception e) {
            e.printStackTrace();
            this.isFull = null;
            typeMistake = true;
        }
    }

    public Boolean isSetIsFull() {
        return isFull != null;
    }


    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        try {
            this.year = year;
        } catch (Exception e) {
            e.printStackTrace();
            this.year = null;
            typeMistake = true;
        }
    }


    public Boolean isSetYear() {
        return year != null;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public Boolean isSetBodyStyle() {
        return bodyStyle != null;
    }

    public Boolean isTypeMistake() {
        return typeMistake;
    }
}
