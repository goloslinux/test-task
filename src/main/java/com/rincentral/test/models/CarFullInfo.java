package com.rincentral.test.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class CarFullInfo extends CarInfo {

    @JsonProperty("body")
    private final BodyCharacteristics bodyCharacteristics;

    @JsonProperty("engine")
    private final EngineCharacteristics engineCharacteristics;

    public CarFullInfo(Integer id, String segment, String brand, String model, String country, String generation, String modification, BodyCharacteristics bodyCharacteristics, EngineCharacteristics engineCharacteristics) {
        super(id, segment, brand, model, country, generation, modification);
        this.bodyCharacteristics=bodyCharacteristics;
        this.engineCharacteristics=engineCharacteristics;
    }
}

