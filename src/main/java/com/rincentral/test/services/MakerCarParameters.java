package com.rincentral.test.services;

import com.rincentral.test.models.BodyCharacteristics;
import com.rincentral.test.models.CarFullInfo;
import com.rincentral.test.models.CarInfo;
import com.rincentral.test.models.EngineCharacteristics;
import org.springframework.stereotype.Service;

@Service
public class MakerCarParameters {

    public BodyCharacteristics getBodyCharacteristics(Integer id, InternalCarsApiService internalCarsApiService) {
        return new BodyCharacteristics(
                internalCarsApiService.getExternalCarsInfoMap().get(id).getBodyHeight()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getBodyLength()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getBodyWidth()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getBodyStyle()
        );
    }

    public EngineCharacteristics getEngineCharacteristics(Integer id, InternalCarsApiService internalCarsApiService) {
        return new EngineCharacteristics(
                internalCarsApiService.getExternalCarsInfoMap().get(id).getFuelType()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getEngineType()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getEngineDisplacement()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getHp()
        );
    }

    public CarInfo getCarInfo(Integer id, InternalCarsApiService internalCarsApiService) {
        return new CarInfo(
                internalCarsApiService.getExternalCarsInfoMap().get(id).getId()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getSegment()
                , internalCarsApiService.getExternalBrandsMap().get(internalCarsApiService.getExternalCarsInfoMap().get(id).getBrandId()).getTitle()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getModel()
                , internalCarsApiService.getExternalBrandsMap().get(internalCarsApiService.getExternalCarsInfoMap().get(id).getBrandId()).getCountry()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getGeneration()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getModification()
        );

    }

    public CarFullInfo getCarFullInfo(Integer id, InternalCarsApiService internalCarsApiService) {
        return new CarFullInfo(
                internalCarsApiService.getExternalCarsInfoMap().get(id).getId()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getSegment()
                , internalCarsApiService.getExternalBrandsMap().get(internalCarsApiService.getExternalCarsInfoMap().get(id).getBrandId()).getTitle()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getModel()
                , internalCarsApiService.getExternalBrandsMap().get(internalCarsApiService.getExternalCarsInfoMap().get(id).getBrandId()).getCountry()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getGeneration()
                , internalCarsApiService.getExternalCarsInfoMap().get(id).getModification()
                , getBodyCharacteristics(
                internalCarsApiService.getExternalCarsInfoMap().get(id).getId()
                , internalCarsApiService)
                , getEngineCharacteristics(
                internalCarsApiService.getExternalCarsInfoMap().get(id).getId()
                , internalCarsApiService)
        );
    }

    public String getCountry(Integer brandId, InternalCarsApiService internalCarsApiService) {
        return internalCarsApiService.getExternalBrandsMap().get(brandId).getCountry();
    }

    public String getBrandTitle(Integer brandId, InternalCarsApiService internalCarsApiService) {
        return internalCarsApiService.getExternalBrandsMap().get(brandId).getTitle();
    }

}
