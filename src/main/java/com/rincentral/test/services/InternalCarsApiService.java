package com.rincentral.test.services;

import com.rincentral.test.models.external.ExternalBrand;
import com.rincentral.test.models.external.ExternalCar;
import com.rincentral.test.models.external.ExternalCarInfo;
import com.rincentral.test.models.external.enums.EngineType;
import com.rincentral.test.models.external.enums.FuelType;
import com.rincentral.test.models.external.enums.GearboxType;
import com.rincentral.test.models.external.enums.WheelDriveType;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class InternalCarsApiService {

    @Getter
    private final Map<Integer, ExternalCarInfo> externalCarsInfoMap = new HashMap<>();
    @Getter
    private final Map<Integer, ExternalBrand> externalBrandsMap = new HashMap<>();
    @Getter
    private final Set<FuelType> fuelTypeSet = new HashSet<>();
    @Getter
    private final Set<String> bodyStylesSet = new HashSet<>();
    @Getter
    private final Set<EngineType> engineTypeSet = new HashSet<>();
    @Getter
    private final Set<WheelDriveType> wheelDriveTypes = new HashSet<>();
    @Getter
    private final Set<GearboxType> gearboxTypeSet = new HashSet<>();


    @Autowired
    public InternalCarsApiService(ExternalCarsApiService externalCarsApiService) {
        List<ExternalCar> externalCars = externalCarsApiService.loadAllCars();
        for (ExternalCar externalCar : externalCarsApiService.loadAllCars()) {
            Integer id = externalCar.getId();
            ExternalCarInfo externalCarInfo = externalCarsApiService.loadCarInformationById(id);
            externalCarsInfoMap.put(id, externalCarInfo);
            fuelTypeSet.add(externalCarInfo.getFuelType());

            for(String style : externalCarInfo.getBodyStyle().split(",")){
                bodyStylesSet.add(style.strip());
            }

            engineTypeSet.add(externalCarInfo.getEngineType());
            wheelDriveTypes.add(externalCarInfo.getWheelDriveType());
            gearboxTypeSet.add(externalCarInfo.getGearboxType());
        }

        for (ExternalBrand externalBrand : externalCarsApiService.loadAllBrands()) {
            externalBrandsMap.put(externalBrand.getId(), externalBrand);
        }
    }
}
