package com.rincentral.test;

import com.rincentral.test.services.InternalCarsApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TestApplication {

	@Autowired
	InternalCarsApiService internalCarsApiService;

	public static void main(String[] args) {SpringApplication.run(TestApplication.class, args);	}

}
